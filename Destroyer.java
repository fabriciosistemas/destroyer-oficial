package destroyer;
import robocode.*;
import robocode.HitRobotEvent;
import robocode.Robot;
import robocode.ScannedRobotEvent;
import robocode.WinEvent;
import static robocode.util.Utils.normalRelativeAngleDegrees;

import java.awt.*;
import java.awt.Color;

/**
 * FBS - a robot by (Fabricio Souza)
 */
public class Destroyer extends Robot
{
	double distancia;
	
	public void run() {
		// Configurando cores
		setBodyColor(new Color(128, 128, 128));
		setGunColor(new Color(50, 50, 50));
		setRadarColor(new Color(200, 70, 70));
		setScanColor(Color.green);
		setBulletColor(Color.yellow);

		// Loop eterno
		while (true) {
			ahead(100); // Avança 100 pixels
   			turnGunRight(360); // escaneia
			back(80); // Regride 80 pixels
			turnGunRight(360); // escaneia
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		double distancia = e.getDistance(); // pega a distância para o robô escaneado
    	if(distancia > 800) // avalia a potência do disparo de acordo com a distância para o alvo
	        fire(1.3);
	    else if(distancia > 600 && distancia <= 800)
	        fire(1.7);
	    else if(distancia > 400 && distancia <= 600)
	        fire(2);
	    else if(distancia > 200 && distancia <= 400)
	        fire(2.5);
	    else if(distancia < 200)
	        fire(3);
	}
	// executa em caso de vitória
	public void onWin(WinEvent e) {
		turnRight(180);
		turnLeft(180);
	}
	// executa ao atingir uma parede
	public void onHitWall(HitWallEvent e){
	    double bearing = e.getBearing();
	    turnRight(-bearing);
	    ahead(100);
	}
}
